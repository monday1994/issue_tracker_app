const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const Issue = sequelize.define("Issue", {
        title : {
            type : DataTypes.STRING
        },
        description: {
            type : DataTypes.STRING,
            unique : true
        },
        state : {
            type : DataTypes.ENUM(VP.ARRAY_OF_ISSUE_STATES)
        }
    }, {
        timestamps: false
    });

    return Issue;
};
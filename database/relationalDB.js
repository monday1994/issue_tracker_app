const util = require('util');
const Promise = require('bluebird');
const Sequelize = require('sequelize');

console.log("db name = "+process.env.DB_NAME);
console.log("db username = "+ process.env.DB_USERNAME);
console.log("db pass = "+ process.env.DB_PASS);
console.log("host = " + process.env.DB_HOST);

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASS, {
    dialect : process.env.DB_DIALECT,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
}, {operatorAliases : false});


// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

const initEntities = function(){

    //Models/tables
    db.User = require('./models/User')(sequelize, Sequelize);
    db.Issue = require('./models/Issue')(sequelize, Sequelize);

    return new Promise((resolve, reject) => {
        //creating relations
        Promise.all([
           //M:N -> User - Issue
            db.User.belongsToMany(db.Issue, {as : 'Issues', through : {model : 'UserIssue', unique : false}, foreignKey : 'UserId'}),
            db.Issue.belongsToMany(db.User, {as : 'Users', through : {model : 'UserIssue', unique : false}, foreignKey : 'IssueId'}),
        ]).then((results)=>{
            resolve(results);
        }).catch(err => {
            console.error('db error = '+util.inspect(err));
            reject(err);
        });
    });
};

db.initDb = function(isForced){
    return new Promise((resolve, reject) => {

        initEntities().then((results)=>{
            db.sequelize.sync({force : isForced}).then(() => {

                if(isForced){
                    let user = {
                        name : 'user',
                        email : 'user@gmail.com',
                    };
                    db.User.create(user).then(result =>{
                         console.log("user has been created");
                    }).catch(err =>{
                        console.log("err occured while admin is being created");
                    });
                }
                console.log("db initialized");
                resolve();
            }).catch(err => {
                console.error(err);
                reject(err);
            });
        }).catch(err=>{
            console.log("error in db initialize");
            reject(err);
        });

    });
};

module.exports = {
    db : db,
    sequelize : sequelize,
    Sequelize : Sequelize
};

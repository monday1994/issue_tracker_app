const express = require('express');
let router = express.Router();
const util = require('util');
const issuesRepo = require('../database/repositories/issuesRepo');

const requestValidator = require('./requestsValidators/issuesRequestsValidator');
const _ = require('lodash');

router.get('/', (req,res,next) => {
    "use strict";

    issuesRepo.getAll().then(issues=>{
        res.render('issues.ejs', {
            issues : issues
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
headers : {

 }
 query : {

 }
 */
router.get('/createIssue', (req,res,next) => {
    res.render('createIssue.ejs');
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    name : 'task',
    description : 'some easy task'
    type : 'OPTION'
 }*/
router.post('/createIssue', (req,res,next) => {
    console.log("body in = ", util.inspect(req.body));
    requestValidator.validateCreateIssue(req).then(result => {

        let newIssue = {
            title : req.body.title,
            description : req.body.description,
            state : req.body.state
        };

        issuesRepo.create(newIssue).then(result => {
            res.redirect('/issues');
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*
 headers : {

 }
 params : {
    id : 1
 }
 */
router.get('/editIssue/:id', (req,res,next) => {
    requestValidator.validateEditIssueParams(req).then(result => {

        let id = req.params.id;

        issuesRepo.getById(id).then(issue=>{
            res.render('editIssue.ejs', {
                issue : issue
            });
        }).catch(err=>{
            return next({status : 400, error : err});
        })
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    id : 1,
    title : 'task',
    description : 'some easy task'
    state : 'OPTION'
 }*/
router.post('/editIssue', (req,res,next) => {
    requestValidator.validateEditIssue(req).then(result => {

        let issueToBeEdit = {
            id : req.body.id,
            title : req.body.title,
            description : req.body.description,
            state : req.body.state
        };

        issuesRepo.update(issueToBeEdit).then(result => {
            res.redirect('/issues');
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*
 headers : {

 }
 params : {
    id : 1
 }
 */
router.get('/removeIssue/:id', (req,res,next) => {
    requestValidator.validateRemoveIssueParams(req).then(result => {

        let id = req.params.id;

        issuesRepo.getById(id).then(issue=>{
            res.render('removeIssue.ejs', {
                issue : issue
            });
        }).catch(err=>{
            return next({status : 400, error : err});
        })
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    id : 1
 }*/
router.post('/removeIssue', (req,res,next) => {
    requestValidator.validateRemoveIssue(req).then(result => {

        let id = req.body.id;

        issuesRepo.removeById(id).then(result => {
            res.redirect('/issues');
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

module.exports = router;
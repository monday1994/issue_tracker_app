const db = require('../relationalDB').db;

exports.create = function(newUser){
    return new Promise((resolve, reject) => {
        db.User.create(newUser).then(createdUser => {
           resolve(createdUser);
        }).catch(err => {
            reject(err.errors);
        });
    });

};

exports.update = function(userToBeUpdate){
    return new Promise((resolve, reject)=>{
        db.User.findOne({where : {id : userToBeUpdate.id}}).then(userFromDb => {
            userFromDb.update(userToBeUpdate).then(updatedUser => {
                resolve(updatedUser);
            }).catch(err =>{
                reject(err);
            })
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        db.User.findOne({where : {id : id}}).then(user => {
            if(user){
                resolve(user);
            } else {
                reject('User with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByIdWithIssues = function(id){
    return new Promise((resolve, reject)=>{
        db.User.findOne({where : {id : id}, include : [{
                model : db.Issue, as : 'Issues'
            }]}).then(user => {
            if(user){
                resolve(user);
            } else {
                reject('User with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getAll = function(){
    return new Promise((resolve, reject)=>{
        db.User.findAll({include : [{
                model : db.Issue, as : 'Issues'
            }]}).then(users => {
            resolve(users);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(id){
    return new Promise((resolve, reject)=>{
        db.User.destroy({where : {id : id}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        });
    });
};
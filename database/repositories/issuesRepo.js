const relationalDb = require('../relationalDB');
const db = relationalDb.db;
const _ = require('lodash');
const async = require('async');
const issueStates = require('../../configuration/validationParameters').ARRAY_OF_ISSUE_STATES;

exports.create = function(newIssue){
    return new Promise((resolve, reject) => {
        db.Issue.create(newIssue).then(createdIssue => {
            resolve(createdIssue);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getAll = function(){
    return new Promise((resolve, reject)=>{
        db.Issue.findAll({include : [{
                model : db.User, as : 'Users'
            }]}).then(issues => {
            resolve(issues);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getAllNotAssociatedWithUser = function(userId){
    let issuesToBeReturn = [];
    return new Promise((resolve, reject)=>{
        db.Issue.findAll({ include : [{
                model : db.User, as : 'Users'
            }]}).then(issues => {
                async.each(issues, (currentIssue, cb)=>{
                    let plainIssue = currentIssue.get({plain : true});
                    let isUserAlreadyAssigned = _.find(plainIssue.Users, { id : Number(userId)});
                    if(!isUserAlreadyAssigned){
                        issuesToBeReturn.push(plainIssue);
                    }
                    cb(null);
                },err=>{
                    if(err){
                        reject(err);
                    } else {
                        resolve(issuesToBeReturn);
                    }
                });
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        db.Issue.findOne({where : {id : id}}).then(issue => {
            if(issue){
                resolve(issue);
            } else {
                reject('Issue with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByUserId = function(userId){
    return new Promise((resolve, reject)=>{
        db.Issue.findAll({where : {UserId : userId}}).then(issues => {
            if(issues){
                resolve(issues);
            } else {
                reject('Issue associated with user : '+userId+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(issueToBeUpdate){
    return new Promise((resolve, reject)=>{
        db.Issue.findOne({where : {id : issueToBeUpdate.id}}).then(issue => {
            if(issue){

                if(issueStates.indexOf(issueToBeUpdate.state) >= issueStates.indexOf(issue.state)){
                    issue.update(issueToBeUpdate).then(updatedIssue => {
                        resolve(updatedIssue);
                    }).catch(err =>{
                        reject(err);
                    })
                } else {
                    reject('State cannot be set back to '+issueToBeUpdate.state+' from '+issue.state);
                }
            } else {
                reject('Issue with id :'+issueToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(issueId){
    return new Promise((resolve, reject) => {
        db.Issue.destroy({where : {id : issueId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};

exports.removeByUserId = function(userId){
    return new Promise((resolve, reject) => {
        db.Issue.destroy({where : {UserId : userId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};
Steps to run project:
1) Create mySQL user : 'user'
2) Create new mySQL schema : 'issue_tracker_db'
3) Give 'user' permission to 'issue_tracker_db'
4) Run 'npm install' command in your terminal
5) Run 'node app.js' command to start the project

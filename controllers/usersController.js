const express = require('express');
let router = express.Router();
const usersRepo = require('../database/repositories/usersRepo');
const issuesRepo = require('../database/repositories/issuesRepo');
const requestValidator = require('./requestsValidators/usersRequestsValidator');
const Promise = require('bluebird');

/*
 headers : {

 }
 query : {

 }
 */
router.get('/', (req,res,next) => {
    "use strict";

    usersRepo.getAll().then(users=>{
        res.render('users.ejs', {
            users : users
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
headers : {

 }
 query : {

 }
 */
router.get('/createUser', (req,res,next) => {
    res.render('createUser.ejs');
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    name : 'John',
    email : 'john@gmail.com'
 }*/
router.post('/createUser', (req,res,next) => {

    requestValidator.validateCreateUser(req).then(result => {

        let newUser = {
            name : req.body.name,
            email : req.body.email
        };

        usersRepo.create(newUser).then(result => {
            res.redirect('/users');
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});


/*
 headers : {

 }
 params : {
    id : 1
 }
 */
router.get('/editUser/:id', (req,res,next) => {
    requestValidator.validateEditUserParams(req).then(result => {

        let id = req.params.id;

        usersRepo.getById(id).then(user=>{
            res.render('editUser.ejs', {
                user : user
            });
        }).catch(err=>{
            return next({status : 400, error : err});
        })
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    id : 1,
    name : 'john',
    email : 'john@gmail.com'
 }*/

router.post('/editUser', (req,res,next) => {
    requestValidator.validateEditUser(req).then(result => {

        let userToBeEdit = {
            id : req.body.id,
            name : req.body.name,
            email : req.body.email
        };

        usersRepo.update(userToBeEdit).then(result => {
            res.redirect('/users');
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*
 headers : {

 }
 params : {
    id : 1
 }
 */
router.get('/removeUser/:id', (req,res,next) => {
    requestValidator.validateRemoveUserParams(req).then(result => {

        let id = req.params.id;

        usersRepo.getById(id).then(user=>{
            res.render('removeUser.ejs', {
                user : user
            });
        }).catch(err=>{
            return next({status : 400, error : err});
        })
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    id : 1
 }*/
router.post('/removeUser', (req,res,next) => {
    requestValidator.validateRemoveUser(req).then(result => {

        let id = req.body.id;

        usersRepo.removeById(id).then(result => {
            res.redirect('/users');
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*
 headers : {

 }
 params : {
    id : 1
 }
 */
router.get('/assignIssue/:id', (req,res,next) => {
    requestValidator.validateAssignIssueParams(req).then(result => {

        let userId = req.params.id;

        Promise.all([
            usersRepo.getById(userId),
            issuesRepo.getAllNotAssociatedWithUser(userId)
        ]).spread((user, issues)=>{
            res.render('assignIssue.ejs', {
                user : user,
                issues : issues
            });
        }).catch(err=>{
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});




/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    id : 1
 }*/
router.post('/assignIssue', (req,res,next) => {
    requestValidator.validateAssignIssue(req).then(result => {

        let userId = req.body.userId;
        let issueId = req.body.issueId;

        Promise.all([
            usersRepo.getById(userId),
            issuesRepo.getById(issueId)
        ]).spread((user, issue)=>{
            user.addIssue(issue).then(result=>{
                res.redirect('/users');
            }).catch(err=>{
                return next({status : 400, error : err});
            })
        }).catch(err=>{
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*
 headers : {

 }
 params : {
    id : 1
 }
 */
router.get('/unassignIssue/:id', (req,res,next) => {
    requestValidator.validateUnassignIssueParams(req).then(result => {

        let userId = req.params.id;

        usersRepo.getByIdWithIssues(userId).then(user => {
            res.render('unassignIssue.ejs', {
                user : user,
                issues : user.Issues
            });
        }).catch(err=>{
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

/*headers : {
    Content-Type : application/x-www-form-urlencoded',
 }
 body : {
    userId : 1,
    issueId : 2
 }*/
router.post('/unassignIssue', (req,res,next) => {
    requestValidator.validateUnassignIssue(req).then(result => {

        let userId = req.body.userId;
        let issueId = req.body.issueId;

        Promise.all([
            usersRepo.getById(userId),
            issuesRepo.getById(issueId)
        ]).spread((user, issue)=>{
            user.removeIssue(issue).then(result=>{
                res.redirect('/users');
            }).catch(err=>{
                return next({status : 400, error : err});
            })
        }).catch(err=>{
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({
            status : 400,
            error : err
        });
    });
});

module.exports = router;
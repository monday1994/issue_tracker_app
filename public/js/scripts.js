function handleRedirectToIssues(e){
    e.preventDefault();
    window.location="http://localhost:3000/issues";
}

function handleRedirectToUsers(e){
    e.preventDefault();
    window.location="http://localhost:3000/users";
}

function validateTextArea(e){
    e.preventDefault();

    var pattern = /^[a-zA-Z0-9ĄĆĘŁŃÓŚŹŻąćęłńóśźż!@#$*_/.\-,\s]+$/;

    let elem = $('#description');

    if(!pattern.test(elem.val()) || elem.val().length > 1000){
        console.log("disabled");
        $('input[type=submit]').prop('disabled', true);
    } else {
        console.log("ok");
        $('input[type=submit]').prop('disabled', false);
    }
}
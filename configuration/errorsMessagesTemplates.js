const invalidTokenErrorMessage = 'Access token is not valid';

module.exports = {
    INVALID_TOKEN_ERROR_MESSAGE : invalidTokenErrorMessage
};
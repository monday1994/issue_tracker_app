module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        name : {
            type : DataTypes.STRING
        },
        email: {
            type : DataTypes.STRING,
            unique : true
        }
    }, {
        timestamps: false
    });

    return User;
};
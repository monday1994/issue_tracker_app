const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreateUser = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkUrlEncodedContentType(req)){
            reject('invalid header type, must be application/x-www-form-urlencoded');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateEditUserParams = function(req){
    return new Promise((resolve, reject) => {

        req.checkParams('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateEditUser = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkUrlEncodedContentType(req)){
            reject('invalid header type, must be application/x-www-form-urlencoded');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateRemoveUserParams = function(req){
    return new Promise((resolve, reject) => {

        req.checkParams('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateRemoveUser = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkUrlEncodedContentType(req)){
            reject('invalid header type, must be application/x-www-form-urlencoded');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateAssignIssueParams = function(req){
    return new Promise((resolve, reject) => {

        req.checkParams('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateAssignIssue = function(req){
    return new Promise((resolve, reject) => {

        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('issueId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUnassignIssueParams = function(req){
    return new Promise((resolve, reject) => {

        req.checkParams('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUnassignIssue = function(req){
    return new Promise((resolve, reject) => {

        req.checkBody('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('issueId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};



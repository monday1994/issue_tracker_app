require('dotenv').load();
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const logger = require('morgan');
const cors = require('cors');
const util = require('util');
const expressValidator = require('express-validator');
let app = express();

//database init
const db = require('./database/relationalDB').db;
//false means sync(force : false)
db.initDb(false);

let server = require('http').Server(app);

const port = process.env.PORT || 3001;
server.listen(port);
console.log("server listens on "+port);

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.use(logger('dev'));

app.use('/public',express.static(path.join(__dirname, '/public')));
app.use(bodyParser.json({limit: '50mb'}));

//below used for handling url-encoded requests
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

app.use(expressValidator());


//controllers
const index = require('./controllers/index');
const usersController = require('./controllers/usersController');
const issuesController = require('./controllers/issuesController');


app.use('/users', usersController);
app.use('/issues', issuesController);
app.use('/', index);


// error handlers below

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        console.log("err at end of app.js in development mode, err = "+util.inspect(err));
        res.status(err.status || 500).json({
            error : err.error
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
    console.log("err at end of app.js in production mode, err = "+util.inspect(err));
    res.status(err.status || 500).json({
        error : err.error
    });
});
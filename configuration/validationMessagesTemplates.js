const VP = require('./validationParameters');

const invalidNameMessage = 'name must contains only alphabet characters and be between '+VP.NAME_MIN_LENGTH+' and '+VP.NAME_MAX_LENGTH+' long';
const invalidEmailMessage = 'email must match pattern email@domain.com and be between '+VP.EMAIL_MIN_LENGTH+' and '+VP.EMAIL_MAX_LENGTH+' long';
const invalidPasswordMessage = 'password must contains only alpha numeric characters and be between '+VP.PASSWORD_MIN_LENGHT+' and '+VP.PASSWORD_MAX_LENGTH+' long';
const invalidIdMessage = 'ID must be integer and be at least 1';


module.exports = {
    INVALID_NAME_MESSAGE : invalidNameMessage,
    INVALID_EMAIL_MESSAGE : invalidEmailMessage,
    INVALID_PASSWORD_MESSAGE : invalidPasswordMessage,
    INVALID_ID_MESSAGE : invalidIdMessage,
};
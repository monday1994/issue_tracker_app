module.exports = {
    onlyAlphabetLetterRegex : /^[a-zA-ZĄĆĘŁŃÓŚŹŻąćęłńóśźż\s]+$/,
    onlyNumbersRegex : /^[0-9]+$/,
    emailRegex : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,
    allCharactersUsedInWriting : /^[a-zA-Z0-9ĄĆĘŁŃÓŚŹŻąćęłńóśźż!@#$*_/.\-,\s]+$/,
    alphaNumeric : /^[a-z0-9]+$/
};


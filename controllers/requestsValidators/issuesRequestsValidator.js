const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreateIssue = function(req){
    return new Promise((resolve, reject) => {

        if (!headersValidator.checkUrlEncodedContentType(req)) {
            reject({
                error: 'invalid header type, must be application/x-www-form-urlencoded'
            });
        }

        req.checkBody('title', 'invalid title, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH, max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('description', 'invalid description, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.DESCRIPTION_MIN_LENGTH, max : VP.DESCRIPTION_MAX_LENGTH
        });

        req.checkBody('state', 'Invalid type, must be one of : '+VP.ARRAY_OF_ISSUE_STATES).isIn(VP.ARRAY_OF_ISSUE_STATES);

        req.getValidationResult().then(result => {
            if (!result.isEmpty()) {
                reject({
                    error: util.inspect(result.array())
                })
            }
            resolve(true);
        });
    });
};

exports.validateEditIssueParams = function(req){
    return new Promise((resolve, reject) => {
        req.checkParams('id', 'invalid issue id').isInt({min : 1});

        req.getValidationResult().then(result => {
            if (!result.isEmpty()) {
                reject({
                    error: util.inspect(result.array())
                })
            }
            resolve(true);
        });
    });
};


exports.validateEditIssue = function(req){
    return new Promise((resolve, reject) => {

        if (!headersValidator.checkUrlEncodedContentType(req)) {
            reject({
                error: 'invalid header type, must be application/x-www-form-urlencoded'
            });
        }

        req.checkBody('id', 'invalid issue id').isInt({min : 1});

        req.checkBody('title', 'invalid title, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH, max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('description', 'invalid description, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.DESCRIPTION_MIN_LENGTH, max : VP.DESCRIPTION_MAX_LENGTH
        });

        req.checkBody('state', 'Invalid type, must be one of : '+VP.ARRAY_OF_ISSUE_STATES).isIn(VP.ARRAY_OF_ISSUE_STATES);

        req.getValidationResult().then(result => {
            if (!result.isEmpty()) {
                reject({
                    error: util.inspect(result.array())
                })
            }
            resolve(true);
        });
    });
};

exports.validateRemoveIssueParams = function(req){
    return new Promise((resolve, reject) => {
        req.checkParams('id', 'invalid issue id').isInt({min : 1});

        req.getValidationResult().then(result => {
            if (!result.isEmpty()) {
                reject({
                    error: util.inspect(result.array())
                })
            }
            resolve(true);
        });
    });
};

exports.validateRemoveIssue = function(req){
    return new Promise((resolve, reject) => {

        if (!headersValidator.checkUrlEncodedContentType(req)) {
            reject({
                error: 'invalid header type, must be application/x-www-form-urlencoded'
            });
        }

        req.checkBody('id', 'invalid issue id').isInt({min : 1});

        req.getValidationResult().then(result => {
            if (!result.isEmpty()) {
                reject({
                    error: util.inspect(result.array())
                })
            }
            resolve(true);
        });
    });
};
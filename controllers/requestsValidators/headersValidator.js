exports.checkJsonContentType = function(req){
    return req.headers['content-type'] === 'application/json';
};

exports.checkUrlEncodedContentType = function(req){
    return req.headers['content-type'] === 'application/x-www-form-urlencoded';
};